package net.korex.betterapi;

import net.korex.betterapi.bettercommand.BetterCommand;
import net.korex.betterapi.bettercommand.BetterCommandManager;
import net.korex.betterapi.betterconfig.MessageElement;
import net.korex.betterapi.betterconfig.MessagesConfig;
import net.korex.betterapi.customevent.packetinjection.PacketInjectionListener;
import net.korex.betterapi.customevent.packetinjection.PacketInjectionManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BetterApi extends JavaPlugin {

    private MessagesConfig messagesConfig;
    private BetterCommandManager betterCommandManager;
    private PacketInjectionManager packetInjectionManager;
    private boolean started;

    public BetterApi() {
        BetterApiManager.add(this);
        this.messagesConfig = new MessagesConfig(this);
        this.betterCommandManager = new BetterCommandManager(this);
        this.packetInjectionManager = new PacketInjectionManager();
        this.started = false;
    }

    public abstract void start();

    public JavaPlugin getJavaPlugin() {
        return this;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        this.start();
        this.started = true;
        BetterApiManager.afterStart();
    }

    @Override
    public void onDisable() {
        for (BetterApi instance : BetterApiManager.getInstances()) {
            if (instance.isEnabled() && !instance.getName().equals("BetterApi")) {
                System.out.println("[BetterApi] Disabling instance: " + instance.getName());
                Bukkit.getPluginManager().disablePlugin(instance);
            }
        }
        super.onDisable();
    }

    public void afterAllStarted() {
    }

    public boolean isStarted() {
        return started;
    }

    public void addMessages(String prefix, MessageElement... messages) {
        this.messagesConfig.addMessages(prefix, messages);
    }

    public void addMessages(MessageElement... messages) {
        this.messagesConfig.addMessages(null, messages);
    }

    public void sendMessage(CommandSender p, MessageElement message, String... args) {
        p.sendMessage(this.getMessage(message, args));
    }

    public String getMessage(MessageElement message, String... args) {
        String prefix = this.getMessagesConfig().getPrefix();
        String messageString = message.replacePlaceholder(prefix, args);
        return messageString;
    }

    public MessagesConfig getMessagesConfig() {
        return messagesConfig;
    }

    public void registerCommand(BetterCommand betterCommand) {
        this.betterCommandManager.register(betterCommand);
    }

    public BetterCommandManager getBetterCommandManager() {
        return betterCommandManager;
    }

    public PacketInjectionManager getPacketInjectionManager() {
        return packetInjectionManager;
    }
}
