package net.korex.betterapi;

import net.korex.betterapi.customevent.packetinjection.PacketInjectionListener;
import net.korex.betterapi.inventory.BetterInventoryListener;
import net.korex.betterapi.inventory.BetterInventoryTest;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class Main extends BetterApi implements Listener {

    public static Main getInstance() {
        return (Main) BetterApiManager.getInstance("BetterApi");
    }

    @Override
    public void start() {
        this.getServer().getPluginManager().registerEvents(new PacketInjectionListener(this.getPacketInjectionManager()), this);
        this.getServer().getPluginManager().registerEvents(new BetterInventoryListener(), this);
    }

    @Override
    public void afterAllStarted() {
        this.getPacketInjectionManager().injectAll();
    }

    @Override
    public void onDisable() {
        super.onDisable();

        this.getPacketInjectionManager().uninjectAll();
    }

}
