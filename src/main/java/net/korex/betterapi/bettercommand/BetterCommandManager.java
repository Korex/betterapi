package net.korex.betterapi.bettercommand;

import net.korex.betterapi.BetterApi;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.List;

public class BetterCommandManager {

    private BetterApi betterApi;

    public BetterCommandManager(BetterApi betterApi) {
        this.betterApi = betterApi;
    }

    public void register(BetterCommand command) {
        try {
            //Registers messages in config
            for (CommandArgument argument : command.getArguments()) {
                for (CommandVariable commandVariable : argument.getCommandVariables()) {
                    commandVariable.getParsingFailedMessage(this.betterApi);
                }
            }


            SimpleCommandMap map = this.getSimpleCommandMap();
            Command parsed = this.parseToCommand(command);
            map.register(this.betterApi.getJavaPlugin().getName(), parsed);

            BukkitCommand bukkitCommand = new BukkitCommand(this.betterApi, command);
            Bukkit.getPluginCommand(command.getName()).setExecutor(bukkitCommand);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public String buildPermission(BetterCommand command, CommandArgument argument) {
        if (argument == null) {
            return this.betterApi.getJavaPlugin().getName() + "." + command.getName();
        }

        if (argument.getName() == null) {
            return this.betterApi.getJavaPlugin().getName() + "." + command.getName() + "." + "origin";
        }

        return this.betterApi.getJavaPlugin().getName() + "." + command.getName() + "." + argument.getName();
    }

    public boolean hasPermission(CommandSender sender, BetterCommand command, CommandArgument argument) {
        if (!command.requiresPermission()) {
            return true;
        }

        String rootPerm = this.buildPermission(command, null);
        String alternativePerm = rootPerm + ".*";

        if (argument == null) {
            return sender.hasPermission(rootPerm) || sender.hasPermission(alternativePerm);
        }

        String specificPerm = this.buildPermission(command, argument);
        return sender.hasPermission(rootPerm) || sender.hasPermission(alternativePerm) || sender.hasPermission(specificPerm);
    }

    private SimpleCommandMap getSimpleCommandMap() throws Exception {
        Object craftServer = Bukkit.getServer();

        System.out.println("***" + craftServer.getClass().getSimpleName());
        Field field = craftServer.getClass().getDeclaredField("commandMap");
        field.setAccessible(true);
        SimpleCommandMap map = (SimpleCommandMap) field.get(craftServer);
        return map;
    }

    private Command parseToCommand(BetterCommand command) throws Exception {
        Constructor<?> constructor = PluginCommand.class.getDeclaredConstructors()[0];
        constructor.setAccessible(true);
        PluginCommand newCmd = (PluginCommand) constructor.newInstance(command.getName(), this.betterApi);
        Object description = command.getDescription();
        List<String> aliases = command.getAliases();

        if (description != null) {
            newCmd.setDescription(description.toString());
        }

        if (aliases != null) {
            newCmd.setAliases(aliases);
        }

        return newCmd;
    }

    public BetterApi getBetterApi() {
        return betterApi;
    }

}
