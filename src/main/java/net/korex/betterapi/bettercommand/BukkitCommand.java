package net.korex.betterapi.bettercommand;

import net.korex.betterapi.BetterApi;
import net.korex.betterapi.betterconfig.MessageElement;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BukkitCommand implements CommandExecutor {

    private BetterCommand betterCommand;
    private BetterApi betterApi;

    private final MessageElement ONLY_FOR_PLAYERS = new MessageElement("CMD_ONLY_FOR_PLAYERS", "&cThis command is only for players.");
    private final MessageElement NO_PERM = new MessageElement("CMD_NO_PERM", "&cYou do not have enough permissions to execute this command!");


    public BukkitCommand(BetterApi betterApi, BetterCommand betterCommand) {
        this.betterCommand = betterCommand;
        this.betterApi = betterApi;

        String prefix = betterApi.getMessagesConfig().getPrefix();

        betterApi.addMessages(prefix, ONLY_FOR_PLAYERS, NO_PERM);
    }


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        boolean isSenderPlayer = commandSender instanceof Player;

        if (!isSenderPlayer && this.betterCommand.onlyForPlayer()) {
            this.betterApi.sendMessage(commandSender, this.ONLY_FOR_PLAYERS);
            return true;
        }

        if (args.length == 0) {
            this.sendHelp(commandSender);
            return true;
        }

        for (CommandArgument argument : this.betterCommand.getArguments()) {
            int length = argument.getName() == null ? argument.getCommandVariables().length : 1 + argument.getCommandVariables().length;

            if (args.length != length) {
                continue;
            }

            if (argument.getName() != null && !args[0].equalsIgnoreCase(argument.getName())) {
                continue;
            }

            if (argument.isOnlyForPlayer() && !isSenderPlayer) {
                this.betterApi.sendMessage(commandSender, ONLY_FOR_PLAYERS);
                return true;
            }

            if (!this.betterApi.getBetterCommandManager().hasPermission(commandSender, this.betterCommand, argument)) {
                continue;
            }

            Object[] passedVariables = new Object[argument.getCommandVariables().length];
            int index = 0;
            boolean success = true;

            for (CommandVariable commandVariable : argument.getCommandVariables()) {
                int argNumber = argument.getName() == null ? index : index + 1;

                if (!commandVariable.canParse(args[argNumber])) {
                    MessageElement parsingFailed = commandVariable.getParsingFailedMessage(this.betterApi);
                    this.betterApi.sendMessage(commandSender, parsingFailed);
                    return true;
                }

                passedVariables[index] = commandVariable.parse(args[argNumber]);
                index = index + 1;
            }

            if (success) {
                Player p = commandSender instanceof Player ? (Player) commandSender : null;
                argument.getRunnable().run(commandSender, p, passedVariables);
                return true;
            }
        }

        this.sendHelp(commandSender);
        return true;
    }

    private void sendHelp(CommandSender sender) {

        List<String> messages = new ArrayList<>();
        messages.add("§3[--------------- §a§l/" + this.betterCommand.getName() + " §r§3---------------]");
        messages.add("§6");

        boolean noPermToExecute = true;
        for (CommandArgument argument : this.betterCommand.getArguments()) {
            if (!this.betterApi.getBetterCommandManager().hasPermission(sender, this.betterCommand, argument)) {
                continue;
            }
            noPermToExecute = false;
            StringBuilder builder = new StringBuilder();
            builder.append("§a/" + this.betterCommand.getName() + " ");
            if (argument.getName() != null) {
                builder.append(argument.getName() + " ");
            }

            for (CommandVariable commandVariable : argument.getCommandVariables()) {
                builder.append("<" + commandVariable.getTypeDescription() + "> ");
            }

            builder.append("§3- " + argument.getDescription());
            messages.add(builder.toString());
            messages.add("§6");
        }

        if (noPermToExecute) {
            this.betterApi.sendMessage(sender, NO_PERM);
        } else {
            for (String message : messages) {
                sender.sendMessage(message);
            }
        }

    }
}
