package net.korex.betterapi.bettercommand;

public class CommandArgument {

    private String name;
    private CommandRunnable runnable;
    private CommandVariable[] commandVariables;
    private String description;
    private boolean onlyForPlayer;

    public CommandArgument(String name, CommandRunnable runnable, String description, CommandVariable... commandVariables) {
        this.name = name;
        this.runnable = runnable;
        this.commandVariables = commandVariables;
        this.description = description;
        this.onlyForPlayer = false;
    }

    public CommandArgument(CommandRunnable runnable, String description, CommandVariable... commandVariables) {
        this(null, runnable, description, commandVariables);
    }

    public String getName() {
        return name;
    }

    public CommandRunnable getRunnable() {
        return runnable;
    }

    public CommandVariable[] getCommandVariables() {
        return this.commandVariables;
    }

    public String getDescription() {
        return description;
    }

    public CommandArgument setOnlyForPlayer() {
        this.onlyForPlayer = true;
        return this;
    }

    public boolean isOnlyForPlayer() {
        return onlyForPlayer;
    }
}
