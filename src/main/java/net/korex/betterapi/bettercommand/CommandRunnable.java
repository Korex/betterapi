package net.korex.betterapi.bettercommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public interface CommandRunnable {

    void run(CommandSender sender, Player player, Object... parsedVariables);

}
