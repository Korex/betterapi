package net.korex.betterapi.bettercommand;

import net.korex.betterapi.BetterApi;
import net.korex.betterapi.betterconfig.MessageElement;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class CommandVariable<T> {

    public static final CommandVariable<Player> PLAYER = new CommandVariable<>("Playername", "%pr &cThe given player is not online!", input -> Bukkit.getPlayer(input));
    public static final CommandVariable<Integer> INTEGER = new CommandVariable<>("Number", "%pr &cPlease enter a number!", input -> {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return null;
        }
    });
    public static final CommandVariable<String> STRING = new CommandVariable<>("Text", null, input -> input);
    public static final CommandVariable<String> NAME = new CommandVariable<>("Name", null, input -> input);


    private String parsingFailedDefault;
    private MessageElement parsingFailedMessageElement;
    private CommandVariableValidator validator;
    private String typeDescription;

    public CommandVariable(String typeDescription, String parsingFailedDefault, CommandVariableValidator validator) {
        this.parsingFailedDefault = parsingFailedDefault;
        this.validator = validator;
        this.typeDescription = typeDescription;
    }

    public CommandVariable cloneWithOtherTypeDescription(String newTypeDescription) {
        CommandVariable variable = new CommandVariable(newTypeDescription, this.parsingFailedDefault, this.validator);
        return variable;
    }

    public MessageElement getParsingFailedMessage(BetterApi betterApi) {
        if(this.parsingFailedDefault == null) {
            return null;
        }

        if(this.parsingFailedMessageElement == null) {
            this.parsingFailedMessageElement = new MessageElement("COMMAND_" + this.typeDescription.toUpperCase() + "_PARSING_FAILED", this.parsingFailedDefault);
            betterApi.addMessages(this.parsingFailedMessageElement);
        }

        return this.parsingFailedMessageElement;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public boolean canParse(String input) {
        return this.validator.parseInput(input) != null;
    }

    public T parse(String input) {
        return (T) this.validator.parseInput(input);
    }

    public interface CommandVariableValidator {

        Object parseInput(String input);

    }
}
