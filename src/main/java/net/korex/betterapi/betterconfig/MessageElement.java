package net.korex.betterapi.betterconfig;

import org.bukkit.ChatColor;

public class MessageElement {

    private String configId;
    private String content;

    public MessageElement(String configId, String content) {
        this.configId = configId;
        this.content = content;
    }

    public String replacePlaceholder(String prefix, String... placeholders) {
        String finalContent = ChatColor.translateAlternateColorCodes('&', this.content);

        if (finalContent.contains("%pr")) {
            finalContent = finalContent.replace("%pr", prefix);
        }

        if (placeholders.length == 1) {
            finalContent = finalContent.replace("%1", placeholders[0]);
            finalContent = finalContent.replace("%", placeholders[0]);
        } else {
            for (int i = 0; i < placeholders.length; i++) {
                finalContent = finalContent.replace("%" + (i + 1), placeholders[i]);
            }
        }

        return finalContent;
    }

    public String getConfigId() {
        return configId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
