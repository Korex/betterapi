package net.korex.betterapi.betterconfig;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class MessagesConfig extends BetterConfig {

    private JavaPlugin javaPlugin;

    public MessagesConfig(JavaPlugin javaPlugin) {
        super(javaPlugin, "messages");
        this.javaPlugin = javaPlugin;
    }

    public String getPrefix() {
        if(!this.getCfg().contains("prefix")) {
            String prefix = "&8[&3" + this.javaPlugin.getName() + "&8]";
            this.getCfg().set("prefix", prefix);
            this.save();
        }
        return this.getValue("prefix");
    }

    public String getValue(String id) {
        String s = this.getCfg().getString(id);
        if(s == null) {
            return null;
        }
        s = ChatColor.translateAlternateColorCodes('&', s);
        return s;
    }

    public void addMessages(String prefix, MessageElement... messages) {
        if(!this.getCfg().contains("prefix")) {
            if(prefix == null) {
                prefix = "&8[&3" + this.javaPlugin.getName() + "&8]";
            }
            this.getCfg().set("prefix", prefix);
        }
        for (MessageElement messageElement : messages) {
            if (this.getCfg().contains(messageElement.getConfigId())) {
                String content = this.getValue(messageElement.getConfigId());
                messageElement.setContent(content);
            } else {
                this.getCfg().set(messageElement.getConfigId(), messageElement.getContent());
            }
        }
        this.save();
    }
}
