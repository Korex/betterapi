package net.korex.betterapi.betteritem;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.Arrays;
import java.util.List;

public class BetterItem {

    private ItemStack item;
    private ItemMeta meta;

    public BetterItem(Material m) {
        this.item = new ItemStack(m);
        this.meta = this.item.getItemMeta();
    }

    public BetterItem(ItemStack item) {
        this.item = item;
        this.meta = this.item.getItemMeta();
    }

    public void setMeta(ItemMeta meta) {
        this.meta = meta;
    }

    public BetterItem setName(String name) {
        this.meta.setDisplayName(name);
        return this;
    }

    public BetterItem setLore(String... lore) {
        List<String> list = Arrays.asList(lore);
        this.meta.setLore(list);
        return this;
    }

    public BetterItem addFlag(ItemFlag... flags) {
        this.meta.addItemFlags(flags);
        return this;
    }

    public ItemStack getItem() {
        if (this.meta == null) {
            return this.item;
        }
        if (this.meta.getDisplayName().contains("&")) {
            this.meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', this.meta.getDisplayName()));
        }
        this.item.setItemMeta(this.meta);
        return item;
    }

    public BetterItem clone() {
        BetterItem betterItem = new BetterItem(this.item);
        betterItem.setMeta(this.meta);
        return betterItem;
    }

    public ItemMeta getMeta() {
        return meta;
    }

    public ItemStack getRawItemStack() {
        return this.item;
    }



}
