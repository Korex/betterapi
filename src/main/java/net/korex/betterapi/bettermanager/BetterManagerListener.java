package net.korex.betterapi.bettermanager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class BetterManagerListener implements Listener {

    private BetterManager<?> betterManager;

    public BetterManagerListener(BetterManager betterManager) {
        this.betterManager = betterManager;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        this.betterManager.load(e.getPlayer().getUniqueId(), null);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.betterManager.write(e.getPlayer().getUniqueId());
        this.betterManager.removeFromCache(e.getPlayer().getUniqueId());
    }

}
