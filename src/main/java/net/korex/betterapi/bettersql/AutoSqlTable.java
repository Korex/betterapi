package net.korex.betterapi.bettersql;

import net.korex.betterapi.bettersql.actions.*;
import net.korex.betterapi.bettersql.annotations.*;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class AutoSqlTable extends SqlTable {

    private Class<?> dataClass;

    public AutoSqlTable(String name, Class<?> dataClass) {
        super(name);
        this.dataClass = dataClass;
    }

    public void create() {
        List<AutoSqlTableColumn> columnList = this.createColumns(null);
        SqlColumn[] columns = new SqlColumn[columnList.size()];

        for (int i = 0; i < columnList.size(); i++) {
            columns[i] = columnList.get(i).getSqlColumn();
        }

        this.setColumns(columns);
        this.create(columns);
    }

    public List<AutoSqlTableColumn> createColumns(Object dataObject) {
        List<AutoSqlTableColumn> columnList = new ArrayList<>();

        for (Field declaredField : this.dataClass.getDeclaredFields()) {
            String fieldName = declaredField.getName();

            declaredField.setAccessible(true);

            Annotation[] annotations = declaredField.getDeclaredAnnotations();
            List<Class<? extends Annotation>> annotationsList = new ArrayList<>();
            for (Annotation annotation : annotations) {
                annotationsList.add(annotation.annotationType());
            }

            SqlColumnType columnType;
            if (annotationsList.contains(SqlCustomType.class)) {
                Annotation annotation = annotations[annotationsList.indexOf(SqlCustomType.class)];
                SqlCustomType sqlCustomType = (SqlCustomType) annotation;
                columnType = sqlCustomType.columnType();
            } else {
                columnType = SqlColumnType.fromType(declaredField.getType());
            }


            SqlColumn column = new SqlColumn(fieldName, columnType);

            if (annotationsList.contains(SqlAvoid.class)) {
                continue;
            }

            if (annotationsList.contains(SqlAutoIncrement.class)) {
                column.setAutoIncrement();
            }

            if (annotationsList.contains(SqlLength.class)) {
                Annotation annotation = annotations[annotationsList.indexOf(SqlLength.class)];
                SqlLength sqlCustomType = (SqlLength) annotation;
                column.setLength(sqlCustomType.length());
            }

            if (annotationsList.contains(SqlNotNull.class)) {
                column.setNotNull();
            }

            if (annotationsList.contains(SqlPrimary.class)) {
                column.setPrimary();
            }

            Object value = null;
            try {
                if (dataObject != null) {
                    declaredField.setAccessible(true);
                    value = declaredField.get(dataObject);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            columnList.add(new AutoSqlTableColumn(declaredField, value, column));
        }

        return columnList;
    }

    public SqlAction<Void> insert(Object dataObject) {
        List<AutoSqlTableColumn> autoSqlTableColumns = this.createColumns(dataObject);

        StringBuilder builder = new StringBuilder();

        int i = 0;

        for (AutoSqlTableColumn autoSqlTableColumn : autoSqlTableColumns) {
            if (i != 0) {
                builder.append(";");
            }
            String value = autoSqlTableColumn.getValue() instanceof String ? "'" + autoSqlTableColumn.getValue().toString() + "'" : autoSqlTableColumn.getValue().toString();
            builder.append(autoSqlTableColumn.getSqlColumn().getName() + "=" + value);
            i = i + 1;
        }

        System.out.println(builder.toString());

        return new SQLTableInsertAction(this, builder.toString());
    }

    public SqlAction<Void> put(Object dataObject) {
        List<AutoSqlTableColumn> autoSqlTableColumns = this.createColumns(dataObject);

        AutoSqlTableColumn primary = null;
        List<AutoSqlTableColumn> value = new ArrayList<>();
        for (AutoSqlTableColumn autoSqlTableColumn : autoSqlTableColumns) {
            if (autoSqlTableColumn.getSqlColumn().isPrimary()) {
                primary = autoSqlTableColumn;
            } else {
                value.add(autoSqlTableColumn);
            }
        }

        if (primary == null) {
            throw new RuntimeException("DataObject " + dataObject.getClass().getName() + " has no primary key. Cannot update object!");
        }

        StringBuilder what = new StringBuilder();
        int i = 0;
        for (AutoSqlTableColumn autoSqlTableColumn : value) {
            if (autoSqlTableColumn.getValue() == null) {
                continue;
            }
            if (i != 0) {
                what.append(";");
            }

            String toAppend = autoSqlTableColumn.getValue().toString();
            what.append(autoSqlTableColumn.getSqlColumn().getName() + "=" + toAppend);
            i = i + 1;
        }

        String where = primary.getSqlColumn().getName() + "=" + (primary.getValue() instanceof String ? "\'" + primary.getValue().toString() + "\'" : primary.getValue().toString());

        return new SQLTablePutAction(this, where, what.toString());
    }

    public SqlAction<Void> update(Object dataObject) {
        List<AutoSqlTableColumn> autoSqlTableColumns = this.createColumns(dataObject);

        AutoSqlTableColumn primary = null;
        List<AutoSqlTableColumn> value = new ArrayList<>();
        for (AutoSqlTableColumn autoSqlTableColumn : autoSqlTableColumns) {
            if (autoSqlTableColumn.getSqlColumn().isPrimary()) {
                primary = autoSqlTableColumn;
            } else {
                value.add(autoSqlTableColumn);
            }
        }

        if (primary == null) {
            throw new RuntimeException("DataObject " + dataObject.getClass().getName() + " has no primary key. Cannot update object!");
        }

        StringBuilder what = new StringBuilder();
        int i = 0;
        for (AutoSqlTableColumn autoSqlTableColumn : value) {
            if (i != 0) {
                what.append(";");
            }
            what.append(autoSqlTableColumn.getSqlColumn().getName() + "=" + autoSqlTableColumn.getValue().toString());
            i = i + 1;
        }

        String where = primary.getSqlColumn().getName() + "=" + primary.getValue().toString();
        return new SQLTableUpdateAction(this, where, what.toString());
    }

    public SQLTableContainsAction contains(String where) {
        return new SQLTableContainsAction(this, where);
    }

    public Class<?> getDataClass() {
        return dataClass;
    }
}










