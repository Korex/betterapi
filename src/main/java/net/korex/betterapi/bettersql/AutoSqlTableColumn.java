package net.korex.betterapi.bettersql;

import java.lang.reflect.Field;

public class AutoSqlTableColumn {

    private Field field;
    private Object value;
    private SqlColumn sqlColumn;

    public AutoSqlTableColumn(Field field, Object value, SqlColumn sqlColumn) {
        this.field = field;
        this.value = value;
        this.sqlColumn = sqlColumn;
    }

    public Field getField() {
        return field;
    }

    public Object getValue() {
        return value;
    }

    public SqlColumn getSqlColumn() {
        return sqlColumn;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AutoSqlTableColumn{");
        sb.append("field=").append(field);
        sb.append(", value=").append(value);
        sb.append(", sqlColumn=").append(sqlColumn);
        sb.append('}');
        return sb.toString();
    }
}
