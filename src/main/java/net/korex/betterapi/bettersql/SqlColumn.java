package net.korex.betterapi.bettersql;

public class SqlColumn {

    private String name;
    private SqlColumnType type;
    private boolean autoIncrement, notNull, primary;
    private Integer length;
    private String foreignKeyTable;
    private String foreignKeyColumn;

    public SqlColumn(String name, SqlColumnType type) {
        this.name = name;
        this.type = type;
        this.notNull = false;
        this.autoIncrement = false;
        this.primary = false;
        this.foreignKeyTable = null;
        this.foreignKeyColumn = null;
    }

    public String getName() {
        return this.name;
    }

    public SqlColumnType getType() {
        return type;
    }

    public Integer getLength() {
        return this.length;
    }

    public SqlColumn setForeignKey(String table, String tableColumn) {
        this.foreignKeyColumn = tableColumn;
        this.foreignKeyTable = table;
        return this;
    }

    public String getForeignKeyColumn() {
        return foreignKeyColumn;
    }

    public String getForeignKeyTable() {
        return foreignKeyTable;
    }

    public SqlColumn setAutoIncrement() {
        this.autoIncrement = true;
        return this;
    }

    public SqlColumn setNotNull() {
        this.notNull = true;
        return this;
    }

    public SqlColumn setLength(int length) {
        this.length = length;
        return this;
    }

    public SqlColumn setPrimary() {
        this.primary = true;
        return this;
    }

    public boolean isAutoIncrement() {
        return this.autoIncrement;
    }

    public boolean isPrimary() {
        return this.primary;
    }

    public String buildStringForQry() {
        StringBuilder builder = new StringBuilder();

        builder.append(this.getName() + " " + this.getType().getNameForQry());
        if (this.getLength() != null) {
            builder.append("(" + this.getLength() + ")");
        }

        if (this.notNull) {
            builder.append(" NOT NULL");
        }

        if (this.isAutoIncrement()) {
            builder.append(" AUTO_INCREMENT");
        }

        return builder.toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SqlColumn{");
        sb.append("name='").append(name).append('\'');
        sb.append(", type=").append(type);
        sb.append(", autoIncrement=").append(autoIncrement);
        sb.append(", notNull=").append(notNull);
        sb.append(", primary=").append(primary);
        sb.append(", length=").append(length);
        sb.append(", foreignKeyTable='").append(foreignKeyTable).append('\'');
        sb.append(", foreignKeyColumn='").append(foreignKeyColumn).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
