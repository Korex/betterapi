package net.korex.betterapi.bettersql;

import java.sql.PreparedStatement;

public class SqlElement<T> {

    public static SqlElement<?> getByString(SqlTable table, String column, String content) {
        if (table.getColumns() == null) {
            throw new NullPointerException("Columns from table are null");
        }

        for (SqlColumn sqlColumn : table.getColumns()) {
            if (sqlColumn.getName().equalsIgnoreCase(column)) {
                SqlColumnType type = sqlColumn.getType();

                if (type == SqlColumnType.TEXT || type == SqlColumnType.VARCHAR || type == SqlColumnType
                        .CHAR) {
                    return new SqlElement<>(content);
                } else if (type == SqlColumnType.INT) {
                    return new SqlElement<>(Integer.parseInt(content));
                } else if (type == SqlColumnType.BIGINT) {
                    return new SqlElement<>(Long.parseLong(content));
                } else if (type == SqlColumnType.BOOLEAN) {
                    boolean bool = Boolean.valueOf(content);
                    return new SqlElement<>(bool);
                } else if (type == SqlColumnType.DOUBLE) {
                    return new SqlElement<>(Double.parseDouble(content));
                } else if (type == SqlColumnType.FLOAT) {
                    return new SqlElement<>(Float.parseFloat(content));
                } else {
                    throw new RuntimeException("Unknown datatype of object");
                }

            }
        }
        return null;
    }

    private T t;

    public SqlElement(T t) {
        this.t = t;
    }

    public T getValue() {
        return this.t;
    }

    public void setToStatement(int location, SqlStatement statement) {
        location = location + 1;
        String s = this.t.getClass().getSimpleName().toLowerCase();

        if(statement == null) {
            System.out.println("(BetterSql) SqlElement#55: SqlStatement is null");
        }

        PreparedStatement preparedStatement = statement.getPreparedStatement();

        if(preparedStatement == null) {
            System.out.println("(BetterSql) SqlElement#61: PreparedStatement is null");
        }

        try {
            if (s.equals("string")) {
                preparedStatement.setString(location, (String) t);
            } else if (s.equals("integer") || s.equals("int")) {
                preparedStatement.setInt(location, (Integer) t);
            } else if (s.equals("long")) {
                preparedStatement.setLong(location, (Long) t);
            } else if (s.equals("boolean")) {
                preparedStatement.setBoolean(location, (Boolean) t);
            } else if (s.equals("short")) {
                preparedStatement.setShort(location, (Short) t);
            } else if (s.equals("byte")) {
                preparedStatement.setByte(location, (Byte) t);
            } else if (s.equals("double")) {
                preparedStatement.setDouble(location, (Double) t);
            } else if (s.equals("float")) {
                preparedStatement.setFloat(location, (Float) t);
            } else {
                throw new RuntimeException("Unknown datatype of object");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
