package net.korex.betterapi.bettersql;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SqlManager {

    private static final SqlManager instance = new SqlManager();

    public static SqlManager getInstance() {
        return instance;
    }

    private List<SqlTable> tableList;
    private Connection connection;
    private long connectionRequestedAt;
    private String host, user, password, database;
    private File sqLiteDirectory;

    private SqlManager() {
        this.tableList = new ArrayList<>();
    }

    public Connection requestConnection() {
        if (this.connection == null) {
            throw new NullPointerException("Connection isnt set.");
        }
        long past = connectionRequestedAt;
        past = past + (1000 * 60 * 60 * 7);
        if (System.currentTimeMillis() > past) {
            try {
                this.connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if(this.sqLiteDirectory != null) {
                this.connect(this.sqLiteDirectory, this.database);
            } else {
                this.connect(this.host, this.user, this.password, this.database);
            }
        }
        return this.connection;
    }

    public SqlTable getTable(String tableName) {
        for (SqlTable sqlTable : this.tableList) {
            if (sqlTable.getName().equalsIgnoreCase(tableName)) {
                return sqlTable;
            }
        }

        SqlTable table = new SqlTable(tableName);
        this.tableList.add(table);
        return table;
    }

    public SqlStatement createStatement(String qry, SqlTable table) {
        return new SqlStatement(qry, table);
    }

    public SqlColumn createColumn(String name, SqlColumnType type) {
        return new SqlColumn(name, type);
    }

    //For sqLite connections (file must end with .db)
    public void connect(File directory, String database) {
        if(!directory.exists()) {
            directory.mkdirs();
        }
        try {
            try {
                Class.forName("org.sqlite.JDBC");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.out.println("Cannot find SQLITE driver");
            }
            String url = "jdbc:sqlite:" + directory.getAbsolutePath() + File.separator + database + ".db";
            Connection connection = DriverManager.getConnection(url);

            if (connection == null) {
                throw new NullPointerException("Connection is null!");
            }

            this.connection = connection;
            this.connectionRequestedAt = System.currentTimeMillis();
            this.host = null;
            this.user = null;
            this.password = null;
            this.database = database;
            this.sqLiteDirectory = directory;
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void connect(String host, String user, String password, String database, int port) {
        try {
            String url = "jdbc:mysql://" + host + ":" + port + "/" + database;

            Connection connection = DriverManager.getConnection(url, user, password);

            if (connection == null) {
                throw new NullPointerException("Connection is null!");
            }

            this.connection = connection;
            this.connectionRequestedAt = System.currentTimeMillis();
            this.host = host;
            this.user = user;
            this.password = password;
            this.database = database;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void connect(String host, String user, String password, String database) {
        this.connect(host, user, password, database, 3306);
    }
}
