package net.korex.betterapi.bettersql;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Set;

public class SqlResultSet {

    private HashMap<Integer, SQLColumnCache> cachedData;

    private ResultSet rs;
    private int next;
    private SqlTable sqlTable;

    public SqlResultSet(SqlTable sqlTable, ResultSet rs) {
        this.next = 0;
        this.rs = rs;
        cachedData = new HashMap<>();
        this.sqlTable = sqlTable;
        try {
            load();
            this.rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int size() {
        return cachedData.keySet().size();
    }

    public boolean foundResult() {
        return this.size() > 0;
    }

    public Object get(String column, int i) {
        return cachedData.get(i).get(column).getValue();
    }

    public <T> T get(int i) {
        if (!(this.sqlTable instanceof AutoSqlTable)) {
            throw new RuntimeException("You have to use the AutoSqlTable to execute this method!");
        }
        AutoSqlTable autoSqlTable = (AutoSqlTable) this.sqlTable;

        SQLColumnCache columnCache = cachedData.get(i);

        if (columnCache == null) {
            return null;
        }

        Object instance = null;

        try {
            Constructor<?> constructor = autoSqlTable.getDataClass().getConstructor(null);
            instance = constructor.newInstance(null);
        } catch (Exception e) {
            throw new RuntimeException("There is not empty constructor in class: " + autoSqlTable.getDataClass().getName());
        }

        for (AutoSqlTableColumn column : autoSqlTable.createColumns(null)) {
            try {
                SqlElement<?> element = columnCache.get(column.getSqlColumn().getName());
                Field field = autoSqlTable.getDataClass().getDeclaredField(column.getSqlColumn().getName());
                field.setAccessible(true);
                field.set(instance, element.getValue());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return (T) instance;
    }

    public Set<String> getColumns() {
        return cachedData.isEmpty() ? null : cachedData.get(0).getColumns();
    }

    private void load() throws Exception {
        int current = 0;
        while (rs.next()) {
            SQLColumnCache cach = new SQLColumnCache();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                String column = rs.getMetaData().getColumnName(i);
                Object obj = rs.getObject(column);
                String content = obj == null ? null : obj.toString();
                SqlElement<?> res = SqlElement.getByString(this.sqlTable, column, content);
                cach.add(column, res);
            }
            cachedData.put(current, cach);
            current++;
        }

    }

    public HashMap<Integer, SQLColumnCache> getCachedData() {
        return cachedData;
    }

    class SQLColumnCache {
        private HashMap<String, SqlElement<?>> cache;

        public SQLColumnCache() {
            cache = new HashMap<>();
        }

        public void add(String column, SqlElement<?> value) {
            cache.put(column, value);
        }

        public SqlElement<?> get(String column) {
            return cache.get(column);
        }

        public Set<String> getColumns() {
            return cache.keySet();
        }
    }


}
