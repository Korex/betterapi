package net.korex.betterapi.bettersql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlStatement {

    private PreparedStatement stmt;
    private Connection connection;
    private SqlTable table;

    public SqlStatement(String qry, SqlTable table) {
        this.table = table;
        this.connection = SqlManager.getInstance().requestConnection();

        try {
            stmt = this.connection.prepareStatement(qry);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public PreparedStatement getPreparedStatement() {
        return this.stmt;
    }

    public void update() {
        try {
            this.stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public SqlStatement set(Object o, int loc) {
        new SqlElement<>(o).setToStatement(loc, this);
        return this;
    }

    public SqlResultSet query() {
        try {
            ResultSet rs = this.stmt.executeQuery();
            SqlResultSet set = new SqlResultSet(this.table, rs);
            return set;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
