package net.korex.betterapi.bettersql;

import net.korex.betterapi.bettersql.actions.*;

import java.util.ArrayList;
import java.util.List;

public class SqlTable {

    private String name;
    private SqlColumn[] columns;

    public SqlTable(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void create(SqlColumn... columns) {
        this.columns = columns;
        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE IF NOT EXISTS " + this.name + " (");
        List<String> primaryKeys = new ArrayList<>();
        List<SqlColumn> foreignKeys = new ArrayList<>();

        for (int i = 0; i < columns.length; i++) {
            SqlColumn column = columns[i];
            if (column.isPrimary()) {
                primaryKeys.add(column.getName());
            } else if (column.getForeignKeyTable() != null && column.getForeignKeyColumn() != null) {
                foreignKeys.add(column);
            }

            String columnToString = column.buildStringForQry();
            builder.append(i != (columns.length - 1) ? columnToString + ", " : columnToString);
        }

        if (primaryKeys.size() > 0) {
            StringBuilder primaryBuilder = new StringBuilder();
            primaryBuilder.append(", PRIMARY KEY(");

            for (int i = 0; i < primaryKeys.size(); i++) {
                primaryBuilder.append(i == (primaryKeys.size() - 1) ? primaryKeys.get(i) : primaryKeys.get(i) + ", ");
            }
            primaryBuilder.append(")");
            builder.append(primaryBuilder.toString());
        }

        if (foreignKeys.size() > 0) {
            //FOREIGN KEY (PersonID) REFERENCES Persons(PersonID)
            StringBuilder foreign = new StringBuilder();

            for (SqlColumn column : foreignKeys) {
                foreign.append(", FOREIGN KEY (" + column.getName() + ") REFERENCES " + column
                        .getForeignKeyTable() + "(" + column.getForeignKeyColumn() + ")");
            }

            builder.append(foreign.toString());
        }


        builder.append(")");
        System.out.println("CREATING QRY: " + builder.toString());
        SqlStatement statement = new SqlStatement(builder.toString(), this);
        statement.update();
    }

    public SqlAction<Void> insert(String content) {
        return new SQLTableInsertAction(this, content);
    }

    public SqlAction<Void> insertIgnore(String content) {
        return new SQLTableInsertIgnoreAction(this, content);
    }

    public SqlAction<SqlResultSet> get(String get, String where) {
        return new SQLTableSelectAction(this, where, get);
    }

    public SqlAction<SqlResultSet> get(String where) {
        return new SQLTableSelectAction(this, where, "*");
    }

    public SqlAction<Void> delete(String where) {
        return new SQLTableDeleteAction(this, where);
    }

    public SqlAction<Void> put(String what, String where) {
        return new SQLTablePutAction(this, where, what);
    }

    public SqlAction<Void> update(String what, String where) {
        return new SQLTableUpdateAction(this, where, what);
    }

    public SQLTableContainsAction contains(String where) {
        return new SQLTableContainsAction(this, where);
    }

    public SqlColumn[] getColumns() {
        return this.columns;
    }

    public void setColumns(SqlColumn[] columns) {
        this.columns = columns;
    }
}
