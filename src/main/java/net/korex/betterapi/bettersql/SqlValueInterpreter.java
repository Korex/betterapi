package net.korex.betterapi.bettersql;

import java.util.LinkedHashMap;

public class SqlValueInterpreter {

    /*FORMAT:   <ColumnName>=<ColumnValue>;<ColumnName>=<ColumnValue>

                column1=value1;value1;column2=value2;value2;value2;column3=100
                [column1][value1;value1;column2][value2;value2;value2;column3][100]

                column1=value1;value1;column2=value2
                [column1][value1;value1;column2][value2]

                column1=value1;
                [column1][value1]*/

    public static LinkedHashMap<String, String> parse(String qry) {
        LinkedHashMap<String, String> cache = new LinkedHashMap<>();
        String[] splitted = qry.split(";");

        if (splitted.length == 1) {
            if (!splitted[0].contains("=")) {
                throw new IllegalArgumentException("Wrong querry!");
            }

            String s = splitted[0];
            String column = s.substring(0, s.indexOf("="));
            String value = s.replaceFirst(column, "").substring(1);
            cache.put(column, value);
            return cache;
        }

        for (String split : splitted) {
            String column = split.substring(0, split.indexOf("="));
            String value = split.replaceFirst(column, "").substring(1);
            cache.put(column, value);
        }

        return cache;
    }

    public static LinkedHashMap<String, String> parse3(String qry) {
        if (!qry.contains("=")) {
            throw new IllegalArgumentException("Wrong querry!");
        }

        LinkedHashMap<String, String> cache = new LinkedHashMap<>();
        String[] qrySplit = qry.split("=");
        if (qrySplit.length < 2) {
            return null;
        }
        if (qrySplit.length == 2) {
            cache.put(qrySplit[0], qrySplit[1]);
            return cache;
        }

        String currentColumnName = null;
        String currentObject;

        for (int i = 0; i < qrySplit.length; i++) {
            if (i == 0) {
                currentColumnName = qrySplit[i];
                continue;
            }

            if (i == qrySplit.length - 1) {
                cache.put(currentColumnName, qrySplit[i]);
                return cache;
            }

            String content = qrySplit[i];

            int lastIndexOf = content.lastIndexOf(";");
            currentObject = content.substring(0, lastIndexOf);
            String second = content.substring(currentObject.length() + 1);

            cache.put(currentColumnName, currentObject);

            currentColumnName = second;
            currentColumnName.replace(";", "");
        }
        return cache;
    }

    public static LinkedHashMap<String, SqlElement<?>> getMap(String content, SqlTable table) {
        LinkedHashMap<String, SqlElement<?>> finalMap = new LinkedHashMap<>();
        LinkedHashMap<String, String> map = SqlValueInterpreter.parse(content);
        for (String key : map.keySet()) {
            String value = map.get(key);
            SqlElement<?> sqlResult = SqlElement.getByString(table, key, value);
            finalMap.put(key, sqlResult);
        }
        return finalMap;
    }


}
