package net.korex.betterapi.bettersql.actions;

import net.korex.betterapi.bettersql.SqlStatement;
import net.korex.betterapi.bettersql.SqlTable;

public class SQLTableDeleteAction extends SqlAction<Void> {

    private String value;

    public SQLTableDeleteAction(SqlTable table, String value) {
        super(table);
        this.value = value;
    }

    public Void action() {
        SqlStatement stmt = this.getWhereSQLStatement("DELETE FROM " + this.getTableName(), this.value);
        System.out.println(stmt.getPreparedStatement().toString());
        stmt.update();
        return null;
    }

}
