package net.korex.betterapi.bettersql.actions;

import net.korex.betterapi.bettersql.SqlTable;

public class SQLTablePutAction extends SqlAction<Void> {

    private String where, what;

    public SQLTablePutAction(SqlTable table, String where, String what) {
        super(table);
        this.what = what;
        this.where = where;
    }

    public Void action() {
        if (new SQLTableContainsAction(super.getTable(), this.where).action()) {
            new SQLTableUpdateAction(super.getTable(), this.where, this.what).action();
        } else {
            new SQLTableInsertAction(super.getTable(), this.where+";"+this.what).action();
        }
        return null;
    }
}
