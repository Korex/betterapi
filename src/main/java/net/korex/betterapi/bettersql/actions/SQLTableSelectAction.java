package net.korex.betterapi.bettersql.actions;

import net.korex.betterapi.bettersql.SqlResultSet;
import net.korex.betterapi.bettersql.SqlStatement;
import net.korex.betterapi.bettersql.SqlTable;

public class SQLTableSelectAction extends SqlAction<SqlResultSet> {

    private String get;
    private String value;

    public SQLTableSelectAction(SqlTable table, String value, String get) {
        super(table);
        this.value = value;
        this.get = get;
    }

    public SqlResultSet action() {
        SqlStatement stmt = this.getWhereSQLStatement("SELECT " + get + " FROM " + this.getTableName(),
                this.value);

        return stmt.query();
    }
}
