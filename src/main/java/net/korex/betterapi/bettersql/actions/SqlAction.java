package net.korex.betterapi.bettersql.actions;

import net.korex.betterapi.bettersql.SqlElement;
import net.korex.betterapi.bettersql.SqlStatement;
import net.korex.betterapi.bettersql.SqlTable;
import net.korex.betterapi.bettersql.SqlValueInterpreter;

import java.util.HashMap;
import java.util.function.Consumer;

public abstract class SqlAction<T> {

    private SqlTable table;

    public SqlAction(SqlTable table) {
        this.table = table;
    }

    public SqlTable getTable() {
        return this.table;
    }

    public String getTableName() {
        return this.getTable().getName();
    }

    protected SqlStatement getWhereSQLStatement(String qryBeforeWhere, String content) {
        HashMap<String, SqlElement<?>> map = SqlValueInterpreter.getMap(content, this.table);
        String qry = this.getQry(qryBeforeWhere + " WHERE", null, map);
        return this.getSQLStatement(qry, map);
    }

    protected String getQry(String qryBefore, String qryAfter, HashMap<String, SqlElement<?>>
            content) {
        StringBuilder builder = new StringBuilder();
        builder.append(qryBefore);
        int i = 0;
        for (String column : content.keySet()) {
            if (i == 0) {
                builder.append(" " + column + " = ?");
            } else {
                builder.append(" AND " + column + " = ?");
            }
            i++;
        }

        if (qryAfter != null) {
            builder.append(qryAfter);
        }

        return builder.toString();
    }

    protected SqlStatement getSQLStatement(String qry, HashMap<String, SqlElement<?>> content) {
        SqlStatement stmt = new SqlStatement(qry, this.table);
        int i = 0;
        for (String column : content.keySet()) {
            content.get(column).setToStatement(i, stmt);
            i++;
        }

        return stmt;
    }

    public T sync() {
        return this.action();
    }

    public void async(Consumer<T> action) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                T t = sync();
                if(action != null) {
                    action.accept(t);
                }
            }
        }).start();

    }

    public abstract T action();

}
