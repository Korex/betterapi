package net.korex.betterapi.customevent.packetinjection;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import net.korex.betterapi.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

public class PacketInjectionManager {

    private List<UUID> listenedPlayers;

    public PacketInjectionManager() {
        this.listenedPlayers = new ArrayList<>();
    }

    public void injectPlayer(Player player) {
        if (!this.areEventsRegistered()) {
            return;
        }

        if (!this.listenedPlayers.contains(player.getUniqueId())) {
            this.listenedPlayers.add(player.getUniqueId());
        }

        System.out.println("[BetterApi] INJECTING PLAYER " + player.getName());
        ChannelDuplexHandler channelDuplexHandler = new CustomChannelDuplexHandler(player);

        Channel channel = getChannel(player);
        channel.pipeline().addBefore("packet_handler", player.getName(), channelDuplexHandler);




        /*ChannelPipeline pipeline = ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline();
        pipeline.addBefore("packet_handler", player.getName(), channelDuplexHandler);*/
    }

    private Channel getChannel(Player player) {
        try {

            Method getHandleMethod = player.getClass().getMethod("getHandle");
            Object entityPlayer = getHandleMethod.invoke(player);

            Field playerConnectionField = entityPlayer.getClass().getField("playerConnection");
            Object playerConnection = playerConnectionField.get(entityPlayer);

            Field networkManagerField = playerConnection.getClass().getField("networkManager");
            Object networkManager = networkManagerField.get(playerConnection);

            Field channelField = networkManager.getClass().getField("channel");
            Object channel = channelField.get(networkManager);

            return (Channel) channel;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void injectAll() {
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            this.injectPlayer(onlinePlayer);
        }
    }

    public void uninjectAll() {
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            this.uninjectPlayer(onlinePlayer);
        }
    }

    public void uninjectPlayer(Player player) {
        if (!this.listenedPlayers.contains(player.getUniqueId())) {
            return;
        }
        this.listenedPlayers.remove(player.getUniqueId());

        Channel channel = this.getChannel(player);
        Callable<Object> callable = () -> {
            Method pipelineMethod = channel.getClass().getMethod("pipeline");
            Object pipeline = pipelineMethod.invoke(channel);
            Method removeMethod = pipeline.getClass().getMethod("remove", String.class);
            removeMethod.invoke(pipeline, player.getName());
            return null;
        };

        channel.eventLoop().submit(callable);


        /*Channel channel2 = ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel;
        channel2.eventLoop().submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                channel2.pipeline().remove(player.getName());
                return null;
            }
        });*/
    }

    private Boolean eventsRegistered;

    public Boolean areEventsRegistered() {
        if (this.eventsRegistered == null) {
            try {
                this.eventsRegistered = this.checkIfEventsAreRegistered();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this.eventsRegistered;
    }

    private boolean checkIfEventsAreRegistered() throws Exception {
        SimplePluginManager pm = (SimplePluginManager) Bukkit.getPluginManager();
        Method method = pm.getClass().getDeclaredMethod("getEventListeners", Class.class);
        method.setAccessible(true);
        HandlerList sendHandlerList = (HandlerList) method.invoke(pm, ServerPacketSendEvent.class);
        HandlerList receiveHandlerList = (HandlerList) method.invoke(pm, ServerPacketReceiveEvent.class);

        if ((sendHandlerList == null && receiveHandlerList == null)
                || (sendHandlerList.getRegisteredListeners() == null && receiveHandlerList.getRegisteredListeners() == null)
                || (sendHandlerList.getRegisteredListeners().length == 0 && receiveHandlerList.getRegisteredListeners().length == 0)) {
            return false;
        }

        return true;
    }

}
