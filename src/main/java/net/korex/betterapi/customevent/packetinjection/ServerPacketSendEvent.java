package net.korex.betterapi.customevent.packetinjection;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ServerPacketSendEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private Object packet;

    public ServerPacketSendEvent(Player player, Object packet) {
        this.player = player;
        this.packet = packet;
    }

    public Player getPlayer() {
        return player;
    }

    public Object getPacket() {
        return packet;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public void setPacket(Object packet) {
        this.packet = packet;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}