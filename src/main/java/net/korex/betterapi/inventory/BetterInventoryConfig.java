package net.korex.betterapi.inventory;

import net.korex.betterapi.BetterApi;
import net.korex.betterapi.betterconfig.BetterConfig;
import net.korex.betterapi.betteritem.BetterItem;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public class BetterInventoryConfig extends BetterConfig {

    public BetterInventoryConfig(BetterApi betterApi, String id) {
        super(betterApi, id);
    }

    public BetterItem getItem(String key, BetterItem defaultItem) {
        BetterItem item = this.getSimpleItemStack(key, defaultItem);
        return item;
    }

    public String getTitle(String defaultTitle) {
        if (this.getCfg().contains("TITLE")) {
            String title = this.getCfg().getString("TITLE");
            title = ChatColor.translateAlternateColorCodes('&', title);
            return title;
        }

        this.getCfg().set("TITLE", defaultTitle);
        this.save();

        return ChatColor.translateAlternateColorCodes('&', defaultTitle);
    }

}
