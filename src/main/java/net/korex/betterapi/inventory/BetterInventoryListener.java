package net.korex.betterapi.inventory;

import net.korex.betterapi.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class BetterInventoryListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player)) {
            return;
        }
        Player player = (Player) e.getWhoClicked();

        BetterInventory betterInventory = BetterInventoryManager.get(player);
        if (betterInventory == null) {
            return;
        }

        if (!betterInventory.isInRulesFreeZone(e.getSlot())) {
            e.setCancelled(true);
        }

        if (e.getClick() == ClickType.UNKNOWN) {
            return;
        }

        BetterInventoryItem.BetterInventoryItemClickEvent event = null;
        for (BetterInventoryItem content : betterInventory.getContents()) {
            if (content.getSlot() == e.getRawSlot()) {
                if (content.getBetterInventoryItemClickEvent() != null) {
                    event = content.getBetterInventoryItemClickEvent();
                }
            }
        }
        if (event != null) {
            event.run(player);
        }

    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (!(e.getPlayer() instanceof Player)) {
            return;
        }
        if (BetterInventoryManager.get((Player) e.getPlayer()) != null) {
            if (BetterInventoryManager.get((Player) e.getPlayer()).canClose()) {
                BetterInventoryManager.remove((Player) e.getPlayer());
            } else {
                Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                    e.getPlayer().openInventory(e.getInventory());
                }, 3);
            }

        }


    }

}
