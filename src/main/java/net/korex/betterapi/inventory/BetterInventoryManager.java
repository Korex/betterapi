package net.korex.betterapi.inventory;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class BetterInventoryManager {

    private static HashMap<UUID, BetterInventory> cache = new HashMap<>();

    public static void put(Player player, BetterInventory betterInventory) {
        if (get(player) != null) {
            return;
        }
        cache.put(player.getUniqueId(), betterInventory);
    }

    public static void remove(Player player) {
        cache.remove(player.getUniqueId());
    }

    public static BetterInventory get(Player player) {
        return cache.get(player.getUniqueId());
    }


}
