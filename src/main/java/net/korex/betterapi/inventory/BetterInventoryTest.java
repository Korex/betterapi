package net.korex.betterapi.inventory;

import net.korex.betterapi.BetterApi;
import net.korex.betterapi.betteritem.BetterItem;
import net.korex.betterapi.inventory.pages.BetterPage;
import net.korex.betterapi.inventory.pages.BetterPagesInventory;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class BetterInventoryTest extends BetterPagesInventory {

    private BetterApi betterApi;

    public BetterInventoryTest(BetterApi betterApi) {
        super(betterApi, "Test");
        this.betterApi = betterApi;
    }


    @Override
    public String getId() {
        return "Test";
    }

    @Override
    public List<BetterPage> pages() {
        return Arrays.asList(new BetterInventoryFirstPage(this),
                new BetterInventorySecondPage(this),
                new BetterInventoryThirdPage(this));
    }
}

class BetterInventoryFirstPage extends BetterPage {

    public BetterInventoryFirstPage(BetterPagesInventory betterPagesInventory) {
        super(betterPagesInventory);
    }


    @Override
    public void setup() {
        this.fillNonConfigurableItem(1, new BetterItem(Material.STONE_AXE));
    }

    @Override
    public void registerEvents() {
        this.registerListener(1, player -> {
            player.sendMessage("First on FIRST PAGE");
        });
    }
}

class BetterInventorySecondPage extends BetterPage {

    public BetterInventorySecondPage(BetterPagesInventory betterPagesInventory) {
        super(betterPagesInventory);
    }


    @Override
    public void setup() {
        this.fillNonConfigurableItem(1, new BetterItem(Material.STONE_AXE));
        this.fillNonConfigurableItem(2, new BetterItem(Material.STONE_AXE));
    }

    @Override
    public void registerEvents() {
        this.registerListener(1, player -> {
            player.sendMessage("First on SECOND PAGE");
        });
        this.registerListener(2, player -> {
            player.sendMessage("Second on SECOND PAGE");
        });
    }
}

class BetterInventoryThirdPage extends BetterPage {
    public BetterInventoryThirdPage(BetterPagesInventory betterPagesInventory) {
        super(betterPagesInventory);
    }

    @Override
    public void setup() {
        this.fillNonConfigurableItem(1, new BetterItem(Material.STONE_AXE));
        this.fillNonConfigurableItem(2, new BetterItem(Material.STONE_AXE));
        this.fillNonConfigurableItem(3, new BetterItem(Material.STONE_AXE));
    }

    @Override
    public void registerEvents() {
        this.registerListener(1, player -> {
            player.sendMessage("First on THIRD PAGE");
        });
        this.registerListener(2, player -> {
            player.sendMessage("Second on THIRD PAGE");
        });
        this.registerListener(3, player -> {
            player.sendMessage("Third on THIRD PAGE");
        });

    }
}
